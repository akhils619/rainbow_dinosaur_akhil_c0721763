package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size suitable for pixel 2
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;





    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------
    // ----------------------------
    // ## SPRITES
    // ----------------------------

        Player player;
        Item item1;
        Item item2;
        Item item3;
        Item item4;

        ArrayList itemList = new ArrayList();
    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------

    int lives = 10;
    int score = 0;


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        this.player = new Player(getContext(), 1600, 120);
        this.item1 = new Item(getContext(), 100, 600);
        item1.setImage(setRandomObjectImage().getImage());

        this.item2 = new Item(getContext(), 100, 400);
        item2.setImage(setRandomObjectImage().getImage());

        this.item3 = new Item(getContext(), 100, 200);
        item3.setImage(setRandomObjectImage().getImage());

        this.item4 = new Item(getContext(), 100, 20);
        item4.setImage(setRandomObjectImage().getImage());



        this.printScreenInfo();
    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public static int generateRandomImage(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {

        this.item1.setxPosition(this.item1.getxPosition() + 20);
        this.item1.updateHitbox();

        this.item2.setxPosition(this.item2.getxPosition() + 20);
        this.item2.updateHitbox();

        this.item3.setxPosition(this.item3.getxPosition() + 20);
        this.item3.updateHitbox();

        this.item4.setxPosition(this.item4.getxPosition() + 20);
        this.item4.updateHitbox();



        if (item1.getxPosition() >= screenWidth){
            item1.setxPosition(100);
            item1.setxPosition(600);
            item1.setImage(setRandomObjectImage().getImage());
        }
        if (item2.getxPosition() >= screenWidth){
            item2.setxPosition(100);
            item2.setxPosition(400);
            item2.setImage(setRandomObjectImage().getImage());

        }
        if (item3.getxPosition() >= screenWidth){
            item3.setxPosition(100);
            item3.setxPosition(200);
            item3.setImage(setRandomObjectImage().getImage());

        }
        if (item4.getxPosition() >= screenWidth){
            item4.setxPosition(100);
            item4.setxPosition(20);
            item4.setImage(setRandomObjectImage().getImage());

        }

        //Collision Detection;

        if(item1.getHitbox().intersect(player.getHitbox())){
            item1.setxPosition(100);
            item1.setxPosition(600);
            item1.setImage(setRandomObjectImage().getImage());
            if (item1.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rainbow32)) ||
                    item1.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.candy32)) ){
                score = score + 1;
            }else  if (item1.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.poop32))  ){
                lives = lives - 1;
            }
        }

        if(item2.getHitbox().intersect(player.getHitbox())){
            item2.setxPosition(100);
            item2.setxPosition(400);
            item2.setImage(setRandomObjectImage().getImage());

            if (item2.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rainbow32)) ||
                    item2.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.candy32)) ){
                score = score + 1;
            }else  if (item2.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.poop32))  ){
                lives = lives - 1;
            }
        }

        if(item3.getHitbox().intersect(player.getHitbox())){
            item3.setxPosition(100);
            item3.setxPosition(200);
            item3.setImage(setRandomObjectImage().getImage());

            if (item3.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rainbow32)) ||
                    item3.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.candy32)) ){
                score = score + 1;
            }else  if (item3.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.poop32))  ){
                lives = lives - 1;
            }
        }

        if(item4.getHitbox().intersect(player.getHitbox())){
            item4.setxPosition(100);
            item4.setxPosition(20);
            item4.setImage(setRandomObjectImage().getImage());
            if (item4.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rainbow32)) ||
                    item4.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.candy32)) ){
                score = score + 1;
            }else  if (item4.getImage().sameAs(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.poop32))  ){
                lives = lives - 1;
            }
        }


            if (this.fingerAction == "bottom") {

            player.setyPosition(player.getyPosition() + 10);


            player.updateHitbox();
        }
        else if (this.fingerAction == "top") {

            player.setyPosition(player.getyPosition() - 10);


            player.updateHitbox();
        }
        if (player.getyPosition() <= 0){
            player.setyPosition(player.getyPosition() + 10);
            player.updateHitbox();
        }
        if (player.getyPosition() >= screenHeight){
            player.setyPosition(player.getyPosition() - 10);
            player.updateHitbox();
        }

    }

    public Item setRandomObjectImage(){
        item1.setImage(item1.getItemList().get(generateRandomImage(0,2)));
        return item1;
    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------
            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            paintbrush.setColor(Color.BLUE);
            this.canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            canvas.drawRect(player.getHitbox(), paintbrush);

            this.canvas.drawBitmap(item1.getImage(),item1.getxPosition(), item1.getyPosition(), paintbrush);
            this.canvas.drawRect(item1.getHitbox(),paintbrush);

            this.canvas.drawBitmap(item2.getImage(),item2.getxPosition(), item2.getyPosition(), paintbrush);
            this.canvas.drawRect(item2.getHitbox(),paintbrush);

            this.canvas.drawBitmap(item3.getImage(),item3.getxPosition(), item3.getyPosition(), paintbrush);
            this.canvas.drawRect(item3.getHitbox(),paintbrush);

            this.canvas.drawBitmap(item4.getImage(),item4.getxPosition(), item4.getyPosition(), paintbrush);
            this.canvas.drawRect(item4.getHitbox(),paintbrush);


            paintbrush.setStyle(Paint.Style.FILL);

            canvas.drawRect(50, 100, this.screenWidth - 300 , 150,paintbrush);
            canvas.drawRect(50, 300, this.screenWidth - 300 , 350,paintbrush);
            canvas.drawRect(50, 500, this.screenWidth - 300 , 550,paintbrush);
            canvas.drawRect(50, 700, this.screenWidth - 300 , 750,paintbrush);

            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);

            //Score Record
            paintbrush.setColor(Color.BLACK);
            paintbrush.setTextSize(60);
            canvas.drawText("Lives:" + lives,
                    this.screenWidth -300,
                    50,
                    paintbrush
            );
            canvas.drawText("Score:" + score,
                    this.screenWidth - 600,
                    50,
                    paintbrush
            );
            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();

        if (userAction == MotionEvent.ACTION_DOWN) {

            float fingerXPosition = event.getX();
            float fingerYPosition = event.getY();

            int middleOfScreen = this.screenHeight / 2;
            if (fingerYPosition <= middleOfScreen) {
                fingerAction = "top";
            }
            else if (fingerYPosition > middleOfScreen) {
                fingerAction = "bottom";
            }
        }
        else if (userAction == MotionEvent.ACTION_UP) {

        }
        return true;
    }
}
